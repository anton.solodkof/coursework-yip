import { Meteor } from 'meteor/meteor';

class ServerApi {
	static toggleChecked({ _id, isChecked }) {
		return Meteor.call('tasks.setIsChecked', _id, !isChecked);
	}

	static deleteTask({ _id }) {
		return Meteor.call('tasks.remove', _id);
	}

	static userRegister(payload) {
		return Meteor.callWithPromise('user.register', payload);
	}

	static createMarkerProfile(payload) {
		return Meteor.callWithPromise('marketProfile.create', payload);
	}

	static createSale(payload) {
		return Meteor.callWithPromise('sale.create', payload);
	}
}

export default ServerApi;
