export function callWithPromise(method, ...params) {
	return new Promise((resolve, reject) => {
		Meteor.call(method, ...params, (err, res) => {
			err ? reject(err) : resolve(res);
		});
	});
}
