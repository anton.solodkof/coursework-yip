import { Mongo } from 'meteor/mongo';

export const ProfileEmailCollection = new Mongo.Collection('ProfileEmail');
