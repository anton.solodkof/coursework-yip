import { Mongo } from 'meteor/mongo';

export const SalePositionCollection = new Mongo.Collection('SalePosition');
