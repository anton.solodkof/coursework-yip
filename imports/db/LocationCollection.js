import { Mongo } from 'meteor/mongo';

export const LocationCollection = new Mongo.Collection('Location');
