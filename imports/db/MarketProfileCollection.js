import { Mongo } from 'meteor/mongo';

export const MarketProfileCollection = new Mongo.Collection('MarketProfile');
