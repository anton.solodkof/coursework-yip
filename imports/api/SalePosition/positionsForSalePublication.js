import { Meteor } from 'meteor/meteor';
import { SalePositionCollection } from '/imports/db/SalePositionCollection';

Meteor.publish('PositionsForSales', function (saleIds) {
	const userId = Meteor.userId();

	if (!userId)
		return this.ready();

	return SalePositionCollection.find({ saleId: { $in: saleIds } });
});
