import { Meteor } from 'meteor/meteor';
import { SaleCollection } from '/imports/db/SaleCollection';

Meteor.publish('AllSales', function () {
	return SaleCollection.find();
});
