import { Meteor } from 'meteor/meteor';
import { SaleCollection } from '/imports/db/SaleCollection';

Meteor.publish('MarketSales', function (marketId) {
	const userId = Meteor.userId();

	if (!userId)
		return this.ready();

	return SaleCollection.find({ marketProfileId: marketId });
});
