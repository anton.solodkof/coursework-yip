import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { MarketProfileCollection } from '/imports/db/MarketProfileCollection';
import { SaleCollection } from '/imports/db/SaleCollection';
import { LocationCollection } from '/imports/db/LocationCollection';
import { SalePositionCollection } from '/imports/db/SalePositionCollection';

Meteor.methods({
	'sale.create'(payload) {
		check(payload, Object);
		check(payload.title, String);
		check(payload.startsAt, String);
		check(payload.endsAt, String);
		check(payload.country, String);
		check(payload.city, String);
		check(payload.address, String);
		check(payload.locationTitle, String);
		check(payload.positions, Array);

		const userId = Meteor.userId();

		if (!userId)
			throw new Meteor.Error('marketProfile.create.not_auth', 'Not authorized');

		const marketProfile = MarketProfileCollection.findOne({ userId });

		if (!marketProfile)
			throw new Meteor.Error('marketProfile.create.already_exists', 'Profile already exists');

		const createdLocationId = LocationCollection.insert({
			title: payload.locationTitle,
			city: payload.city,
			country: payload.country,
			address: payload.address,
		});

		const createdSaleId = SaleCollection.insert({
			marketProfileId: marketProfile._id,
			title: payload.title,
			startsAt: payload.startsAt,
			endsAt: payload.endsAt,
			locationId: createdLocationId,
		});

		const positionsToInsert = payload.positions.map(position => {
			return {
				saleId: createdSaleId,
				...position
			}
		});

		positionsToInsert.forEach(position => {
			SalePositionCollection.insert(position);
		});		

		return createdSaleId;
	},
});
