import { Meteor } from 'meteor/meteor';
import { MarketProfileCollection } from '/imports/db/MarketProfileCollection';

Meteor.publish('MarketProfile', function () {
	const userId = Meteor.userId();

	if (!userId)
		return this.ready();

	return MarketProfileCollection.find({ userId });
});
