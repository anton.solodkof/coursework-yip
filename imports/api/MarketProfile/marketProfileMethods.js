import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { MarketProfileCollection } from '/imports/db/MarketProfileCollection';
import { ProfileEmailCollection } from '/imports/db/ProfileEmailCollection';

Meteor.methods({
	'marketProfile.create'(payload) {
		check(payload, Object);
		check(payload.marketName, String);
		check(payload.marketEmail, String);

		const userId = Meteor.userId();

		if (!userId)
			throw new Meteor.Error('marketProfile.create.not_auth', 'Not authorized');

		const isUserExists = !!MarketProfileCollection.findOne({ marketName: payload.marketName });

		if (isUserExists)
			throw new Meteor.Error('marketProfile.create.already_exists', 'Profile already exists');

		const createdProfileId = MarketProfileCollection.insert({
			userId,
			marketName: payload.marketName,
		});

		ProfileEmailCollection.insert({
			profileId: createdProfileId,
			email: payload.marketEmail,
			isVerified: false,
		})

		return createdProfileId;
	},
});
