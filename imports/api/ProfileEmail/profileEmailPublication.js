import { Meteor } from 'meteor/meteor';
import { ProfileEmailCollection } from '/imports/db/ProfileEmailCollection';

Meteor.publish('ProfileEmail', function () {
	const userId = Meteor.userId();

	if (!userId)
		return this.ready();

	return ProfileEmailCollection.find({ profileId: userId });
});
