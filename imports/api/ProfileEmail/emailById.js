import { Meteor } from 'meteor/meteor';
import { ProfileEmailCollection } from '/imports/db/ProfileEmailCollection';

Meteor.publish('ProfileEmailById', function (profileId) {
	return ProfileEmailCollection.find({ profileId });
});
