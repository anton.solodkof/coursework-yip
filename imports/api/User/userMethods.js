import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { ProfileEmailCollection } from '/imports/db/ProfileEmailCollection';

Meteor.methods({
	'user.register'(payload) {
		check(payload, Object);
		check(payload.username, String);
		check(payload.password, String);
		check(payload.phoneNumber, String);

		const isUserExists = !!Accounts.findUserByUsername(payload.username);

		if (isUserExists)
			throw new Meteor.Error('user.register.already_exists', 'User already exists');

		const createdUserId = Accounts.createUser({
			username: payload.username,
			password: payload.password,
			profile: {
				phoneNumber: payload.phoneNumber,
			}
		});

		if (payload.email) {
			ProfileEmailCollection.insert({
				profileId: createdUserId,
				email: payload.email,
				isVerified: false,
			})
		}

		return createdUserId;
	},
});
