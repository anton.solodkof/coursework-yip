import { Meteor } from 'meteor/meteor';
import React from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { Switch, Route } from 'react-router-dom';
import { Header, LoginForm, SalesList, UserProfile, CreateMarketProfile, MarketProfile, CreateSale } from './components';

export const App = () => {
	const user = useTracker(() => Meteor.user());

	return (
		<div className="app">
			<Header user={user} />
			<div className="main">
				<Switch>
					<Route path='/login' component={LoginForm} />
					<Route path='/profile' component={UserProfile} />
					<Route path='/market-profile' component={MarketProfile} />
					<Route path='/create-market-profile' component={CreateMarketProfile} />
					<Route path='/create-sale' component={CreateSale} />
					<Route path='/markets' component={LoginForm} />
					<Route exact path='/' component={SalesList} />
				</Switch>
			</div>
		</div>
	)
};
