import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import ServerApi from '/imports/utils/ServerApi';

const CreateMarketProfile = () => {
	const history = useHistory();
	const [errorText, setErrorText] = useState('');
	const [saleName, setSaleName] = useState('');
	const [startsAt, setStartsAt] = useState('');
	const [endsAt, setEndsAt] = useState('');
	const [locationTitle, setLocationTitle] = useState('');
	const [country, setCountry] = useState('');
	const [city, setCity] = useState('');
	const [address, setAddress] = useState('');
	const [positions, setPositions] = useState([]);
	const [positionTitle, setPositionTitle] = useState('');
	const [positionPriceBefore, setPositionPriceBefore] = useState('');
	const [positionPrice, setPositionPrice] = useState('');

	const submit = e => {
		e.preventDefault();

		if (!positions.length) {
			return setErrorText('Добавьте хотя бы одну позицию');
		}

		const payload = {
			title: saleName,
			startsAt,
			endsAt,
			country,
			city,
			address,
			locationTitle,
			positions,
		}

		ServerApi.createSale(payload)
			.then(() => {
				history.push('/market-profile');
			})
			.catch(error => setErrorText(error.reason));
	};

	const addPosition = () => {
		if (positionTitle && positionPriceBefore && positionPrice) {
			const newPositions = [...positions, {
				title: positionTitle,
				priceBefore: positionPriceBefore,
				price: positionPrice
			}];
	
			setPositions(newPositions);
			setPositionTitle('');
			setPositionPriceBefore('');
			setPositionPrice('');
		}
	}

	const handleValueChange = (value, handler) => {
		setErrorText('');
		handler(value);
	}

	return (
		<form onSubmit={submit} className="login-form">
			<div>
				<label htmlFor="saleName">Название распродажи</label>
				<input
					type="text"
					placeholder="Название распродажи"
					name="saleName"
					required
					value={saleName}
					onChange={e => handleValueChange(e.target.value, setSaleName)}
				/>
			</div>
			<div>
				<label htmlFor="startsAt">Начало</label>
				<input
					type="date"
					placeholder="Начало"
					name="startsAt"
					required
					value={startsAt}
					onChange={e => handleValueChange(e.target.value, setStartsAt)}
				/>
			</div>
			<div>
				<label htmlFor="endsAt">Конец</label>
				<input
					type="date"
					placeholder="Конец"
					name="endsAt"
					required
					value={endsAt}
					onChange={e => handleValueChange(e.target.value, setEndsAt)}
				/>
			</div>
			<div>
				{positions.map((position, index) => (
					<div key={`${position.name}-${index}`} className='text-block'>{position.title}: <span style={{ textDecoration: 'line-through'}}>{position.priceBefore}</span> {position.price}</div>
				))}
			</div>
			<div>
				<label htmlFor="positionTitle">Название позиции</label>
				<input
					type="text"
					placeholder="Название позиции"
					name="positionTitle"
					value={positionTitle}
					onChange={e => handleValueChange(e.target.value, setPositionTitle)}
				/>
				<label htmlFor="positionPriceBefore">Цена до распродажи</label>
				<input
					type="text"
					placeholder="Цена до распродажи"
					name="positionPriceBefore"
					value={positionPriceBefore}
					onChange={e => handleValueChange(e.target.value, setPositionPriceBefore)}
				/>
				<label htmlFor="positionPrice">Цена во время распродажи</label>
				<input
					type="text"
					placeholder="Цена во время распродажи"
					name="positionPrice"
					value={positionPrice}
					onChange={e => handleValueChange(e.target.value, setPositionPrice)}
				/>
				<div className="button" onClick={addPosition}>Добавить позицию</div>
			</div>
			<div>
				<label htmlFor="marketName">Место проведения</label>
				<input
					type="text"
					placeholder="Местоположение"
					name="marketName"
					required
					value={locationTitle}
					onChange={e => handleValueChange(e.target.value, setLocationTitle)}
				/>
			</div>
			<div>
				<label htmlFor="country">Страна</label>
				<input
					type="text"
					placeholder="Страна"
					name="country"
					required
					value={country}
					onChange={e => handleValueChange(e.target.value, setCountry)}
				/>
			</div>
			<div>
				<label htmlFor="city">Город</label>
				<input
					type="text"
					placeholder="Город"
					name="city"
					required
					value={city}
					onChange={e => handleValueChange(e.target.value, setCity)}
				/>
			</div>
			<div>
				<label htmlFor="address">Адрес</label>
				<input
					type="text"
					placeholder="Адрес"
					name="address"
					required
					value={address}
					onChange={e => handleValueChange(e.target.value, setAddress)}
				/>
			</div>
			<div>
				{errorText && <div style={{ marginBottom: '10px' }} className="error-text">{errorText}</div>}
				<button type="submit">Создать распрожажу</button>
			</div>
		</form>
	);
};

export default CreateMarketProfile;
