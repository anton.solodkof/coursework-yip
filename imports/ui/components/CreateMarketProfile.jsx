import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import ServerApi from '/imports/utils/ServerApi';

const CreateMarketProfile = () => {
	const history = useHistory();
	const [errorText, setErrorText] = useState('');
	const [marketName, setMarketName] = useState('');
	const [marketEmail, setMarketEmail] = useState('');

	const submit = e => {
		e.preventDefault();

		const payload = {
			marketName,
			marketEmail,
		};

		ServerApi.createMarkerProfile(payload)
			.then(() => {
				history.push('/market-profile');
			})
			.catch(error => setErrorText(error.reason));
	};

	const handleValueChange = (value, handler) => {
		setErrorText('');
		handler(value);
	}

	return (
		<form onSubmit={submit} className="login-form">
			<div>
				<label htmlFor="marketName">Название продавца</label>
				<input
					type="text"
					placeholder="Название продавца"
					name="marketName"
					required
					value={marketName}
					onChange={e => handleValueChange(e.target.value, setMarketName)}
				/>
			</div>
			<div>
				<label htmlFor="marketName">Почта продавца</label>
				<input
					type="text"
					placeholder="Почта продавца"
					name="marketEmail"
					required
					value={marketEmail}
					onChange={e => handleValueChange(e.target.value, setMarketEmail)}
				/>
			</div>
			<div>
				{errorText && <div style={{ marginBottom: '10px' }} className="error-text">{errorText}</div>}
				<button type="submit">Создать профиль продавца</button>
			</div>
		</form>
	);
};

export default CreateMarketProfile;
