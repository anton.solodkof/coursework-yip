/* eslint-disable react/prop-types */
import React from 'react';
import { Link } from 'react-router-dom';

const Header = ({ user }) => {
	return (
		<header>
			<div className="app-bar">
				<div className="app-header">
					<h1>SalesRadar</h1>
					{user
						? <Link to='/profile' className="user">
							{user.username}
						</Link>
						: <Link to='/login' className="user">Log In</Link>
					}
				</div>
			</div>
		</header>
	)
}

export default Header;
