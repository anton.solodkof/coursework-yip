import { Meteor } from 'meteor/meteor';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import ServerApi from '/imports/utils/ServerApi';

const LoginForm = () => {
	const history = useHistory();
	const [isRegistration, setIsRegistration] = useState(false);
	const [errorText, setErrorText] = useState('');
	const [phoneNumber, setPhoneNumber] = useState('');
	const [email, setEmail] = useState('');
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');

	const submit = e => {
		e.preventDefault();

		if (isRegistration) {
			const payload = {
				username,
				phoneNumber,
				password,
			}

			if (email.length) {
				payload.email = email;
			}

			ServerApi.userRegister(payload)
				.then(() => {
					Meteor.loginWithPassword(username, password, (error) => {
						if (error) {
							setErrorText(error.reason)
						}

						history.replace('/');
					});
				})
				.catch(error => setErrorText(error.reason))
		} else {
			Meteor.loginWithPassword(username, password, (error) => {
				if (error) {
					setErrorText(error.reason)
				}

				history.replace('/');
			});
		}
	};

	const handleValueChange = (value, handler) => {
		setErrorText('');
		handler(value);
	}

	const handleSetRegistration = isRegistration => {
		setPhoneNumber('');
		setErrorText('');
		setEmail('');
		setUsername('');
		setPassword('');
		setIsRegistration(isRegistration);
	}

	return (
		<form onSubmit={submit} className="login-form">
			<div>
				<label htmlFor="username">Username</label>
				<input
					type="text"
					placeholder="Username"
					name="username"
					required
					value={username}
					onChange={e => handleValueChange(e.target.value, setUsername)}
				/>
			</div>
			{isRegistration
				&& <div>
					<label htmlFor="tel">Phone Number</label>
					<input
						type="tel"
						placeholder="Phone number"
						name="tel"
						required
						value={phoneNumber}
						onChange={e => handleValueChange(e.target.value, setPhoneNumber)}
					/>
				</div>
			}
			<div>
				<label htmlFor="password">Password</label>
				<input
					type="password"
					placeholder="Password"
					name="password"
					required
					value={password}
					onChange={e => handleValueChange(e.target.value, setPassword)}
				/>
			</div>
			{isRegistration
				&& <div>
					<label htmlFor="email">Email</label>
					<input
						type="email"
						placeholder="Email"
						name="email"
						value={email}
						onChange={e => handleValueChange(e.target.value, setEmail)}
					/>
				</div>
			}
			<div>
				{errorText && <div style={{ marginBottom: '10px' }} className="error-text">{errorText}</div>}
				<button type="submit">{isRegistration ? 'Register' : 'Log In'}</button>
				{!isRegistration && <div style={{ marginTop: '10px' }} className="link-button" onClick={() => handleSetRegistration(true)}>Registration</div>}
				{isRegistration && <div style={{ marginTop: '10px' }} className="link-button" onClick={() => handleSetRegistration(false)}>Log In</div>}
			</div>
		</form>
	);
};

export default LoginForm;
