/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import { SaleCollection } from '/imports/db/SaleCollection';
import { SalePositionCollection } from '/imports/db/SalePositionCollection';

const SalesList = () => {
	const { allSales, isLoaded: isSalesLoaded } = useTracker(() => {
		const handler = Meteor.subscribe('AllSales');

		if (!handler.ready())
			return { allSales: [], isLoaded: false };

		const allSales = SaleCollection.find().fetch();

		return { allSales, isLoaded: true }
	});

	const { salesPositions, isLoaded: isPositionsLoaded } = useTracker(() => {
		if (!allSales.length)
			return { salesPositions: [], isLoaded: false };

		const positionsIds = allSales.map(sale => sale._id);

		const handler = Meteor.subscribe('PositionsForSales', positionsIds);

		if (!handler.ready())
			return { salesPositions: [], isLoaded: false };

		const salesPositions = SalePositionCollection.find().fetch();

		return { salesPositions, isLoaded: true };
	});

	const formSalesList = () => {
		const sales = allSales.map((sale) => {
			const positions = salesPositions.filter(position => position.saleId === sale._id);

			const positionsList = positions.map(position => (
				<li key={position._id}>
					{position.title}: <span style={{ textDecoration: 'line-through'}}>{position.priceBefore}</span> {position.price}
				</li>
			));

			return (
				<li style={{ display: 'flex', flexDirection: 'column'}} key={sale._id}>
					<h3>{sale.title}</h3>
					<p>С {sale.startsAt} до {sale.endsAt}</p>
					<ul>
						{positionsList}
					</ul>
				</li>
			)
		});

		return (
			<ul className="sales">
				{sales}
			</ul>
		)
	}

	return (
		<Fragment>
			{(!isPositionsLoaded || !isSalesLoaded)
				? <div className="loading">loading...</div>
				: formSalesList()
			}
		</Fragment>
	)
}

export default SalesList;
