/* eslint-disable react/prop-types */
import React from 'react';
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import { ProfileEmailCollection } from '/imports/db/ProfileEmailCollection';
import { MarketProfileCollection } from '/imports/db/MarketProfileCollection';
import { SaleCollection } from '/imports/db/SaleCollection';
import { SalePositionCollection } from '/imports/db/SalePositionCollection';

const MarketProfile = () => {
	const { marketProfile, isLoaded } = useTracker(() => {
		const handler = Meteor.subscribe('MarketProfile');

		if (!handler.ready())
			return { isLoaded: false };

		const marketProfile = MarketProfileCollection.findOne();

		return { marketProfile, isLoaded: true }
	});

	const marketSales = useTracker(() => {
		if (!marketProfile)
			return [];

		const handler = Meteor.subscribe('MarketSales', marketProfile._id);

		if (!handler.ready())
			return [];

		const marketSales = SaleCollection.find().fetch();

		return marketSales
	});

	const salesPositions = useTracker(() => {
		if (!marketSales?.length)
			return [];

		const salesIds = marketSales.map(sale => sale._id);

		const handler = Meteor.subscribe('PositionsForSales', salesIds);

		if (!handler.ready())
			return [];

		const salesPositions = SalePositionCollection.find().fetch();

		return salesPositions
	});

	console.log(marketSales);
	console.log(salesPositions);

	const emails = useTracker(() => {
		if (!marketProfile)
			return [];

		const handler = Meteor.subscribe('ProfileEmailById', marketProfile._id);

		if (!handler.ready())
			return [];

		const emails = ProfileEmailCollection.find().fetch();

		return emails
	});

	const emailsToRender = emails.map((emailObj) => (
		<div className="text-block" key={emailObj.email}>Почта : {emailObj.email}</div>
	));

	const hasMarketProfile = isLoaded && !!marketProfile;

	return (
		<div className="center-container">
			{!!hasMarketProfile
				&& <>
					<div className="text-block">Имя продавца: {marketProfile.marketName}</div>
					{emailsToRender}
					<Link to='/create-sale' className='text-block link-button'>Создать распродажу</Link>
				</>
			}
		</div>
	)
}

export default MarketProfile;
