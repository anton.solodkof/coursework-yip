/* eslint-disable react/prop-types */
import React from 'react';
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import { Link, Redirect } from 'react-router-dom';
import { ProfileEmailCollection } from '/imports/db/ProfileEmailCollection';
import { MarketProfileCollection } from '/imports/db/MarketProfileCollection';

const UserProfile = () => {
	const account = Meteor.user();

	const emails = useTracker(() => {
		const handler = Meteor.subscribe('ProfileEmail');

		if (!handler.ready())
			return [];

		const emails = ProfileEmailCollection.find().fetch();

		return emails
	});

	const emailsToRender = emails.map((emailObj) => (
		<div className="text-block" key={emailObj.email}>Почта : {emailObj.email}</div>
	));

	const { marketProfile, isLoaded } = useTracker(() => {
		const handler = Meteor.subscribe('MarketProfile');

		if (!handler.ready())
			return { isLoaded: false };

		const marketProfile = MarketProfileCollection.findOne();

		return { marketProfile, isLoaded: true }
	});

	const hasMarketProfile = isLoaded && !!marketProfile;

	return (
		<div className="center-container">
			{!!account
				? <>
					<div className="text-block">Имя пользователя: {account.username}</div>
					{!!account.profile?.phoneNumber && <div className="text-block">Телефон: {account.profile.phoneNumber}</div>}
					{emailsToRender}
					{hasMarketProfile
						? <Link to='/market-profile' className='text-block link-button'>Открыть профиль продавца</Link>
						: <Link to='/create-market-profile' className='text-block link-button'>Создать профиль продавца</Link>
					}
					<div onClick={() => Meteor.logout()} style={{ cursor: 'pointer' }} className="error-text">Выход</div>
				</>
				: <Redirect to='/' />
			}
		</div>
	)
}

export default UserProfile;
